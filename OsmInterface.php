<?php

class OsmInterface
{
    protected $host = 'https://www.openstreetmap.org';
    protected $cookie;

    public function login($user, $pass)
    {
        $this->cookie = array();
        $url = $this->host.'/login';
        $page = file_get_contents($url);
        foreach($http_response_header as $hrh) {
            $hrh = explode(': ', $hrh);
            if($hrh[0] != 'Set-Cookie') continue;
            $hrh = explode(';', $hrh[1]);
            $cok = explode('=', $hrh[0]);
            $this->setCookie($cok[0], $cok[1]);
        }
        //var_dump($this->cookie);

        $xml = simplexml_load_string($page);
        $form = $xml->body->div->div[2]->div->div->form;
        //$url = (string)$form->attributes()['action'];
        $authenticity_token = (string)$form->input[1]->attributes()['value'];
        $referer = (string)$form->input[2]->attributes()['value'];

        $data = array(
            'authenticity_token' => $authenticity_token,
            'referer' => $referer,
            'username' => $user,
            'password' => $pass,
            'commit' => 'Anmelden',
        );
        $page = $this->postRequest('/login', $data);
    }

    public function setCookie($name, $value)
    {
        $this->cookie[$name] = $value;
    }

    protected function getCookieString()
    {
        return implode('; ', $this->cookie);
    }

    public function getRequest($path)
    {
        $head = 'Cookie: ' . $this->getCookieString() . "\r\n";
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => $head,
                'method'  => 'GET',
            ),
        );
        $url = $this->host.$path;
        $context  = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    public function postRequest($path, $data)
    {
        // Convert the data array into URL Parameters like a=b&foo=bar etc.
        $data = http_build_query($data);
        $head = "Content-type: application/x-www-form-urlencoded\r\n";
        $head .= 'Cookie: ' . $this->getCookieString() . "\r\n";
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => $head,
                'method'  => 'POST',
                'content' => $data,
            ),
        );
        $url = $this->host.$path;
        $context  = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }
}
